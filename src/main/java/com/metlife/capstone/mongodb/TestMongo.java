package com.metlife.capstone.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;



import static java.util.concurrent.TimeUnit.SECONDS;


public class TestMongo {
	private MongoClient mongoClient;
	private DB db;
	private Set<String> colls;
	private DBCollection coll;
	
	public TestMongo() throws UnknownHostException{
		config();
		showIt();
	}
	
	private void config() throws UnknownHostException{
		mongoClient = new MongoClient( "localhost");
		db = mongoClient.getDB( "test" );
		colls = db.getCollectionNames(); 
		coll = db.getCollection("javaDb");
		coll.drop();
		coll.find().maxTime(1, SECONDS).count();
	}
	
	private void showIt(){
		printCollections();
		addObject();
		findFirst();
		addObjects("Pick Me!");
		findNumbers();
		DBCursor cursor = find();
		BasicDBObject query = findMe(cursor, "Pick Me!");
		findUs(query,cursor, "Pick Me!");
	}
	
	private void printCollections(){
		for (String s : colls) {
		    System.out.println(s);
		}
	}
	
	private void addObject(){
	BasicDBObject doc = new BasicDBObject("name", "MongoDB")
    .append("type", "database")
    .append("count", 1)
    .append("info", new BasicDBObject("x", 203).append("y", 102));
	coll.insert(doc);
	}
	
	private void addObjects(String info){
		for (int i=0; i < 100; i++) {
		    coll.insert(new BasicDBObject(info, i));
		}
	}
	
	private void findFirst(){
		DBObject myDoc = coll.findOne();
		System.out.println(myDoc);
	}
	
	private void findNumbers(){
		System.out.println(coll.getCount());
	}
	
	private DBCursor find(){
		DBCursor cursor = coll.find();
		try {
		   while(cursor.hasNext()) {
		       System.out.println(cursor.next());
		   }
		} finally {
		   cursor.close();
		}
		return cursor;
	}
	
	private BasicDBObject findMe(Cursor cursor, String queryName){
		BasicDBObject query = new BasicDBObject(queryName, 71);

		cursor = coll.find(query);

		try {
		   while(cursor.hasNext()) {
		       System.out.println(cursor.next());
		   }
		} finally {
		   cursor.close();
		}
		query = new BasicDBObject("j", new BasicDBObject("$ne", 3))
        .append("k", new BasicDBObject("$gt", 10));

		cursor = coll.find(query);

		try {
			while(cursor.hasNext()) {
				System.out.println(cursor.next());
			}
		} finally {
			cursor.close();
		}
		return query;
	}
	
	private void findUs(BasicDBObject query, Cursor cursor, String queryName)
	{
		query = new BasicDBObject(queryName, new BasicDBObject("$gt", 50));

		cursor = coll.find(query);
		try {
		    while (cursor.hasNext()) {
		        System.out.println(cursor.next());
		    }
		} finally {
		    cursor.close();
		}
	}
}
